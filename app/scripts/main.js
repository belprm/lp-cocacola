/* jshint devel:true */
/* global Swiper */
/* global Modernizr */

;(function ( $, window, document, undefined ) {
    'use strict';

    var $document = $(document),
        $radio = $('.radio'),
        $form = $('.form'),
        swiper,
        debug = false;

    $document.on('ready',function(){
        initSwiper();
        initRadio();
        initForm();
        initNote();
    });

    function initSwiper() {
        swiper = new Swiper('.swiper-container', {
            autoplay: 5000,
            grabCursor: true
        });
    }

    function initRadio() {
        var $radioLabel = $radio.find('.radio__label'),
            $radioInput = $radio.find('.radio__input');

        $radioLabel.on('click', function() {
            $radioLabel.removeClass('radio__label--checked');
            $radioInput.each(function() {
                var $this = $(this);

                if ($this.prop('checked') === true) {
                    $this.closest($radioLabel).addClass('radio__label--checked');
                }
            });
        });
    }

    function initForm() {
        $.validator.addMethod('regexp', function(value, element, param) {
                return this.optional(element) || value.match(param);
            });

        $form.find('.form__container').each(function(){
           var $this = $(this);

            $this.validate({
                debug: debug,
                rules: {
                    firstname: {
                        required: true
                    },
                    lastname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    country: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    phone: {
                        required: true,
                        regexp: /^[+]?[\d\s-\(\)]{6,}$/g
                    },
                    captcha: {
                        required: true
                    }
                },
                messages: {
                    firstname: {
                        required: 'Укажите свое имя'
                    },
                    lastname: {
                        required: 'Укажите свою фамилию'
                    },
                    email: {
                        required: 'Укажите свою почту',
                        email: 'Укажите почту полностью'
                    },
                    country: {
                        required: 'Укажите страну'
                    },
                    city: {
                        required: 'Укажите город'
                    },
                    phone: {
                        required: 'Укажите телефон',
                        regexp: 'Укажите телефон полностью'
                    },
                    captcha: {
                        required: 'Укажите код'
                    }
                }
            });
        });
    }

    function initNote() {
        var $note = $('.note');

        $note.each(function() {
            var $this = $(this),
                $title = $this.find('.note__title'),
                $popup = $this.find('.note__popup');

            if (Modernizr.touch) {
                $popup.addClass('note__popup--visible note__popup--static');
            } else {
                $title.hover(function() {
                $popup.addClass('note__popup--visible');
                }, function() {
                    $popup.removeClass('note__popup--visible');
                });
            }

        });
    }

})( jQuery, window, document );
